package danieltribeiro.xpenses;

import java.util.Map;

import javax.print.attribute.standard.Media;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/transfs")
public class TransfResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Path("notfound")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> notFound() {
        throw new NotFoundException();
    } 
}